package net.ideasam.ane
{
	import flash.external.ExtensionContext;
	import flash.system.Capabilities;
	
	public class MessageANE
	{
		public static const SHORT:int	= 0;
		public static const LONG:int	= 1;
		
		private static const extensionID:String = "net.ideasam.ane.MessageANE";
		private static var _context:ExtensionContext;
		
		public function MessageANE()
		{
	
		}
		
		public static function get isSupported():Boolean
		{
			var result:Boolean = (Capabilities.manufacturer.search('iOS') > -1 || Capabilities.manufacturer.search('Android') > -1);
			trace('MessageANE is'+(result ? ' ' : ' not ')+'supported');
			return result;
		}
		
		public static function showToast(message:String, duration:int):void
		{
			if(!isSupported) return;
			
			getContext().call("showToast", message, duration);
		}
		
		private static function getContext():ExtensionContext
		{
			if(null == _context)
			{
				_context = ExtensionContext.createExtensionContext(extensionID, null);
			}
			return _context
		}
	}
}