package net.ideasam.ane;
import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREExtension;


public class MessageAneExtension implements FREExtension {

	public static MessageAneContext context;
	@Override
	public FREContext createContext(String arg0) {
		Log.d("MessageAneContext", "call createContext");
		return context = new MessageAneContext();
	}

	@Override
	public void dispose() {
	}

	@Override
	public void initialize() {
	}

}
