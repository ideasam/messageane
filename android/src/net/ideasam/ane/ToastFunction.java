package net.ideasam.ane;
import android.util.Log;
import android.widget.Toast;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREInvalidObjectException;
import com.adobe.fre.FREObject;
import com.adobe.fre.FRETypeMismatchException;
import com.adobe.fre.FREWrongThreadException;


public class ToastFunction implements FREFunction {

	@Override
	public FREObject call(FREContext ctx, FREObject[] str) {
		
		String msg = "";
		int duration = 0;
		
		try {
			msg = str[0].getAsString();
			duration = str[1].getAsInt();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (FRETypeMismatchException e) {
			e.printStackTrace();
		} catch (FREInvalidObjectException e) {
			e.printStackTrace();
		} catch (FREWrongThreadException e) {
			e.printStackTrace();
		}
		
		Log.d("ToastFunction", msg);
		
		Toast.makeText(ctx.getActivity(), msg, duration).show();
		
		return null;
	}

}
