//
//  MessageANE.m
//  MessageANE
//
//  Created by 전 병훈 on 2015. 5. 12..
//  Copyright (c) 2015년 IDEASAM inc. All rights reserved.
//

#import "MessageANE.h"
#import "WToast.h"


@implementation MessageANE

void MessageAneExtInitializer(void** extDataToSet, FREContextInitializer* ctxInitializerToSet,
                              FREContextFinalizer* ctxFinalizerToSet)
{
    *extDataToSet = NULL;
    *ctxInitializerToSet = &MessageAneContextInitializer;
    *ctxFinalizerToSet = &MessageAneContextFinalizer;
}

void MessageAneContextInitializer(void *extData, const uint8_t* ctxType, FREContext ctx,
                                  uint32_t* numFunctionsToTest, const FRENamedFunction** functionsToSet)
{
    int nbFuntionsToLink = 1;
    
    *numFunctionsToTest = nbFuntionsToLink;
    
    FRENamedFunction* func = (FRENamedFunction*) malloc(sizeof(FRENamedFunction) * nbFuntionsToLink);
    func[0].name = (const uint8_t*) "showToast";
    func[0].functionData = NULL;
    func[0].function = &showToast;
    
    NSLog( @"init ANE - 0" );
    
    *functionsToSet = func;
    
    NSLog( @"init ANE - 1" );
}

void MessageAneContextFinalizer(FREContext ctx)
{
    
}

void MessageAneExtFinalizer(void* extData)
{
    return;
}

FREObject showToast(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[] )
{
    if(argc<2 || !argv || !argv[0] || !argv[1])
        return nil;
    
    uint32_t messageLength;
    const uint8_t *message = nil;
    double dur;
    FREGetObjectAsDouble(argv[1], &dur);
    
    if(FREGetObjectAsUTF8(argv[0], &messageLength, &message)==FRE_OK){
        if(message){
            [WToast showWithText:[NSString stringWithUTF8String:(char*)message]];
        }
    }
    NSLog( @"showToast%s", message);
    return nil;
}
@end
